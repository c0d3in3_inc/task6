package com.c0d3in3.task6

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.task6.databinding.ItemLayoutBinding
import com.c0d3in3.task6.model.ItemModel

class ItemAdapter(private val items: ArrayList<ItemModel>, private val callback : CustomCallback) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    interface CustomCallback{
        fun deleteItem(position: Int, item : ItemModel)
        fun readItem(item : ItemModel)
        fun editItem(position: Int, item : ItemModel)
    }
    inner class ViewHolder(private val binding : ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        fun onBind(){

            if(items.isNotEmpty()){
                val item = items[adapterPosition]
                binding.itemModel = item

                binding.deleteButton.setOnClickListener {
                    callback.deleteItem(adapterPosition, item)
                }

                binding.editButton.setOnClickListener {
                    callback.editItem(adapterPosition, item)
                }

                binding.readButton.setOnClickListener {
                    callback.readItem(item)
                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding : ItemLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_layout, parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}