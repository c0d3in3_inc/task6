package com.c0d3in3.task6

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object DataBindingComponents {

    @JvmStatic
    @BindingAdapter("setImage")
    fun setImage(view: ImageView, url: String){
        Glide.with(view).load(url).into(view)
    }

    @JvmStatic
    @BindingAdapter("setId","setTitle")
    fun setTitle(view: TextView, id: Int, title : String){
        view.text = "$id. $title"
    }
}