package com.c0d3in3.task6.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ItemViewModel : ViewModel() {

    val count : MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun init(){
        count.value = 0
    }

    fun getSize() : Int {
        return count.value!!
    }

    fun setCount(size : Int){
        count.value = size
    }
}