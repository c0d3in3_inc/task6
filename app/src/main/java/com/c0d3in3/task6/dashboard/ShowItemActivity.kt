package com.c0d3in3.task6.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.c0d3in3.task6.DataBindingComponents
import com.c0d3in3.task6.R
import com.c0d3in3.task6.databinding.FullItemLayoutBinding
import com.c0d3in3.task6.databinding.ItemLayoutBinding
import com.c0d3in3.task6.model.ItemModel

class ShowItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : FullItemLayoutBinding = DataBindingUtil.setContentView(this, R.layout.full_item_layout)
        binding.itemModel  = intent.extras?.get("itemModel") as ItemModel
    }
}
