package com.c0d3in3.task6.dashboard

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.c0d3in3.task6.App
import com.c0d3in3.task6.ItemAdapter
import com.c0d3in3.task6.R
import com.c0d3in3.task6.model.ItemModel
import com.c0d3in3.task6.room_database.AppDatabase
import com.c0d3in3.task6.room_database.Item
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity(), ItemAdapter.CustomCallback {

    private val model = ItemViewModel()

    companion object{
        private val db = Room.databaseBuilder(
            App.getInstance().applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()

        const val ADD_ITEM = 22
        const val EDIT_ITEM = 23
    }

    private val itemsList = arrayListOf<ItemModel>()
    private val adapter = ItemAdapter(itemsList, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        itemsRecyclerView.layoutManager = LinearLayoutManager(this)
        itemsRecyclerView.adapter = adapter

        model.init()
        loadItems()

        addButton.setOnClickListener {
            startAddActivity()
        }

        searchButton.setOnClickListener {
            val title = searchEditText.text.toString()
            if(title.isBlank()) return@setOnClickListener Toast.makeText(this, "Please fill search field", Toast.LENGTH_SHORT).show()
            findItem(title)
        }

        val countObserver = Observer<Int>{
            if(model.getSize() == 0){
                emptyTextView.visibility = View.VISIBLE
                itemsRecyclerView.visibility = View.GONE
            }
            else{
                emptyTextView.visibility = View.GONE
                itemsRecyclerView.visibility = View.VISIBLE
            }
        }
        model.count.observe(this, countObserver)
    }

    private fun addItemToDB(item : ItemModel){
        AsyncTask.execute{
            val mItem = Item(0, item.title, item.description, item.url)
            db.itemDao().insertAll(mItem)
            runOnUiThread { model.setCount(itemsList.size+1) }
        }
    }

    override fun deleteItem(position : Int, item : ItemModel){
        AsyncTask.execute{
            itemsList.remove(item)
            val mItem = db.itemDao().findByTitle(item.title!!)
            db.itemDao().delete(mItem)
            runOnUiThread { model.setCount(itemsList.size); adapter.notifyItemRemoved(position)}
        }
    }

    override fun readItem(item: ItemModel) {
        findItem(item.title!!)
    }

    override fun editItem(position: Int, item: ItemModel) {
        val intent = Intent(this, AddActivity::class.java)
        intent.putExtra("itemModel", item)
        intent.putExtra("position", position)
        startActivityForResult(intent, EDIT_ITEM)
    }

    private fun loadItems(){
        AsyncTask.execute{
            val items = db.itemDao().getAll()
            items.forEach {
                itemsList.add(ItemModel(it.uid, it.title, it.description, it.url))
                runOnUiThread { model.setCount(itemsList.size);adapter.notifyItemInserted(itemsList.size-1) }
            }
        }
    }

    private fun findByID(id : Int){
        var item : Item?
        AsyncTask.execute{
            item = db.itemDao().findById(id)
            if(item != null){
                val intent = Intent(this, ShowItemActivity::class.java)
                val itemModel = ItemModel(item!!.uid, item!!.title, item!!.description, item!!.url)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("itemModel", itemModel)
                startActivity(intent)
            }
            else{
                runOnUiThread{
                    Toast.makeText(this, "Item with given ID couldn't be found!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun findByTitle(title: String){
        var item : Item?
        AsyncTask.execute{
            item = db.itemDao().findByTitle(title)
            if(item != null){
                val intent = Intent(this, ShowItemActivity::class.java)
                val itemModel = ItemModel(item!!.uid, item!!.title, item!!.description, item!!.url)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("itemModel", itemModel)
                startActivity(intent)
            }
            else{
                runOnUiThread{
                    Toast.makeText(this, "Item with given title couldn't be found!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun findItem(text: String){
        try{
            val id = text.toInt()
            findByID(id)
        }
        catch (e: NumberFormatException){
            findByTitle(text)
        }
    }

    private fun startAddActivity(){
        val intent = Intent(this, AddActivity::class.java)
        startActivityForResult(intent, ADD_ITEM)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == ADD_ITEM){
            val itemModel = data?.extras?.get("itemModel") as ItemModel
            addItemToDB(itemModel)
            AsyncTask.execute{
                val item = db.itemDao().findByTitle(itemModel.title!!)
                itemsList.add(ItemModel(item.uid, item.title, item.description, item.url))
                runOnUiThread{ adapter.notifyItemInserted(itemsList.size-1)}
            }
        }

        if(resultCode == Activity.RESULT_OK && requestCode == EDIT_ITEM){
            val itemModel = data?.extras?.get("itemModel") as ItemModel
            val position = data?.extras?.get("position") as Int
            itemsList[position] = itemModel
            adapter.notifyItemChanged(position)
            AsyncTask.execute{
                db.itemDao().editItem(itemModel.id!!, itemModel.title!!, itemModel.description!!, itemModel.url!!)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}


