package com.c0d3in3.task6.dashboard

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.c0d3in3.task6.R
import com.c0d3in3.task6.model.ItemModel
import kotlinx.android.synthetic.main.activity_add.*
import kotlin.math.max

class AddActivity : AppCompatActivity() {

    companion object{
        const val minTitle = 5
        const val maxTitle = 30
        const val minDesc = 32
        const val maxDesc = 300
    }

    var editing = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        init()
    }

    private fun init(){
        if(intent.hasExtra("itemModel")){
            val itemModel = intent.extras?.get("itemModel") as ItemModel
            titleEditText.setText(itemModel.title)
            descriptionEditText.setText(itemModel.description)
            urlEditText.setText(itemModel.url)
            addButton.text = "Edit"
            editing = true
        }
        else addButton.text = getString(R.string.add)
        addButton.setOnClickListener {
            if(!editing) addItem()
            else saveItem()
        }

        cancelButton.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
    }

    private fun addItem(){
        val title = titleEditText.text.toString()
        val desc = descriptionEditText.text.toString()
        val url = urlEditText.text.toString()

        if(title.isBlank() || desc.isBlank() || url.isBlank()) return Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
        if(title.length < minTitle || title.length > maxTitle) return Toast.makeText(this, "Title should be in range of $minTitle-$maxTitle  symbols", Toast.LENGTH_SHORT).show()
        if(desc.length < minDesc || desc.length > maxDesc) return Toast.makeText(this, "Description should be in range of $minDesc-$maxDesc symbols", Toast.LENGTH_SHORT).show()

        val intent = intent
        val itemModel = ItemModel(0, title, desc, url)

        intent.putExtra("itemModel", itemModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun saveItem(){
        val title = titleEditText.text.toString()
        val desc = descriptionEditText.text.toString()
        val url = urlEditText.text.toString()

        if(title.isBlank() || desc.isBlank() || url.isBlank()) return Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
        if(title.length < minTitle || title.length > maxTitle) return Toast.makeText(this, "Title should be in range of $minTitle-$maxTitle  symbols", Toast.LENGTH_SHORT).show()
        if(desc.length < minDesc || desc.length > maxDesc) return Toast.makeText(this, "Description should be in range of $minDesc-$maxDesc symbols", Toast.LENGTH_SHORT).show()

        val intent = intent
        val item = intent.extras!!.get("itemModel") as ItemModel
        val itemModel = ItemModel(item.id, title, desc, url)

        intent.removeExtra("itemModel")
        intent.putExtra("itemModel", itemModel)
        intent.putExtra("position", intent.getIntExtra("position", 0))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
