package com.c0d3in3.task6.room_database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ItemDao {
    @Query("SELECT * FROM item")
    fun getAll(): List<Item>

    @Query("SELECT * FROM item WHERE uid IN (:itemIds)")
    fun loadAllByIds(itemIds: IntArray): List<Item>

    @Query("SELECT * FROM item WHERE title LIKE :title LIMIT 1")
    fun findByTitle(title: String): Item

    @Query("SELECT * FROM item WHERE uid LIKE :id LIMIT 1")
    fun findById(id: Int): Item

    @Insert
    fun insertAll(vararg items: Item)

    @Query("UPDATE item SET title = :title, description = :description, url = :url WHERE uid = :id")
    fun editItem(id: Int, title: String, description : String, url : String)

    @Delete
    fun delete(item: Item)
}